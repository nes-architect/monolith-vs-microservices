# Déployer via les user datas AWS

```bash
#!/bin/bash
export DEBIAN_FRONTEND=noninteractive 
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install -y nodejs git
sudo apt purge apache2 -y

mkdir -p /app
cd /app

git clone https://gitlab.com/nes-architect/monolith-vs-microservices.git .

npm i
npm run build
PORT=80 npm run start

exit 0;
```