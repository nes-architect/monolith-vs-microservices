import Express from "express";
import morgan from "morgan";
import cookieParser from "cookie-parser";
import Api from "./api";

export default class Server {

    public app : Express.Application;

    constructor(){
        this.app = Express();
        this.configure();
    }

    /**
     * Start the server on with the prot defined by PORT variable or 3000 if not found
     * It listen on all IPS if not defined
     * @param listenOn 
     * @param port 
     */
    public start(listenOn : string = "0.0.0.0", port : number = <number><unknown>process.env.PORT || 3000){
        this.app.listen(port, listenOn, () => {
            console.log("Server is running on port ",port);
        });
    }

    /**
     * Configure the expressjs with middlewares
     */
    private configure(){
        this.app.use( morgan('dev') );                          //-> Display http server access logs in the console
        this.app.use( Express.json() );                         //-> Allow JSON parse in req.body
        this.app.use( Express.urlencoded({extended: true}) );   //-> Allow URLEncoded in req.body
        this.app.use( cookieParser() );                         //-> Enable feature for cookie parsing in express

        // Bind all routes to the express endpoints
        Api.bind(this.app);

        // Expose the entire folder "public" to the endoint /web
        this.app.use("/web", Express.static(`${__dirname}/../public`) );

        // Redirect "/" to "/web"
        this.app.get("/", (req,res) => {
            res.redirect("/web");
        });

    }

}