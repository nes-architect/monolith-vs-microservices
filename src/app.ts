import Post from "./models/posts";
import Topic from "./models/topics";
import User from "./models/users";
import Server from "./server";

async function main(){

    // Load collections
    User.load();
    Topic.load();
    Post.load();

    // Start the server
    let server = new Server();
    server.start();
}

main();