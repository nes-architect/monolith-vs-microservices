import fs from "fs";

interface objInterface {
    id?: string,
    createdAt?: string
    user_id: string,
    title: string,
}

export default class Topic {

    public static datas : objInterface[] = [];
    public props : objInterface;

    constructor(props : objInterface){
        this.props = props;
        if(!props.id) this.props.id = Date.now().toString();
        if(!props.createdAt) this.props.createdAt = (new Date()).toLocaleString();
    }

    public async save(){
        let index : number = Topic.datas.map( u => u.id ).indexOf(this.props.id);
        if( index > -1 ){
            await Topic.update(index, this.props);
            Topic.write();
        }
        else{
            Topic.datas.push( Object.assign( {}, this.props) );
            Topic.write();
        }
    }

    public async delete(){
        let index : number = Topic.datas.map( u => u.id ).indexOf(this.props.id);
        if(index === -1) return false;

        Topic.datas.splice(index, 1);
        Topic.write();

    }

    private static async update(indexDatas : number, datas : objInterface){
        Topic.datas[indexDatas] = datas;
    }

    public static write(){
        fs.writeFileSync(`${__dirname}/../../data/${this.name}.json`, JSON.stringify(this.datas, null, "\t"));
        this.load();
    }

    public static load(){
        let filename : string = `${__dirname}/../../data/${this.name}.json`; 
        if(fs.existsSync(filename)){
            let rawDatas = fs.readFileSync(<fs.PathOrFileDescriptor>filename).toString();
            this.datas = JSON.parse(rawDatas);
        }
    }

} 