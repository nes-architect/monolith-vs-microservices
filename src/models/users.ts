/**
 * Comments are sames for Posts.ts and Topics.ts
 */

// Import FS to perform filesystem operations
import fs from "fs";

/**
 * Define the properties of the item
 */
interface objInterface {
    id?: string,        // An ID generated from a timestamp
    createdAt?: string, // A date in human readable format
    username: string,   // the username
    password: string    // a weak password in plain text... don't put serious datas on it
}

export default class User {

    // Contains all items of the database class
    public static datas : objInterface[] = [];
    // Local properties of the item
    public props : objInterface;

    /**
     * The constructor of the item, take props parameters
     * @param props 
     */
    constructor(props : objInterface){
        this.props = props;

        // IF no ID, we generate a new one with a timestamp
        if(!props.id) this.props.id = Date.now().toString();
        // Same for the createdAt with a date in locale string format
        if(!props.createdAt) this.props.createdAt = (new Date()).toLocaleString();
    }

    /**
     * Save the item in the datas of the class
     * If already exists, call the update method isntead (internaly)
     */
    public async save(){

        // Username already taken ? If yes return an error
        let usernameExists = User.datas.map( u => u.username ).indexOf(this.props.username);
        if(usernameExists > -1) throw "Username already exists";

        // The item had an ID ? If yes we will try to update it
        let index : number = User.datas.map( u => u.id ).indexOf(this.props.id);
        if( index > -1 ){
            await User.update(index, this.props);
            User.write();
        }
        // Else we save it normally
        else{

            // Push in the datas props of the class (not the instance of the class)
            User.datas.push( Object.assign( {}, this.props) );
            // Save it in file
            User.write();
        }
    }

    /**
     * Delete the item from the datas of the class and from the saved file
     * @returns 
     */
    public async delete(){
        
        // If item exists ? not ? return false
        let index : number = User.datas.map( u => u.id ).indexOf(this.props.id);
        if(index === -1) return false;

        // If yes, we find it with his index and remove it from the datas of the class and save the file
        User.datas.splice(index, 1);
        User.write();

    }

    /**
     * Update the itel from his index into the datas of the class
     * @param indexDatas 
     * @param datas 
     */
    private static async update(indexDatas : number, datas : objInterface){
        User.datas[indexDatas] = datas;
    }

    /**
     * Write datas of the class in JSON format in file
     */
    public static write(){
        fs.writeFileSync(`${__dirname}/../../data/${this.name}.json`, JSON.stringify(this.datas, null, "\t"));
        this.load();
    }

    /**
     * Load datas from the file into datas of the class
     */
    public static load(){
        let filename : string = `${__dirname}/../../data/${this.name}.json`; 
        if(fs.existsSync(filename)){
            let rawDatas = fs.readFileSync(<fs.PathOrFileDescriptor>filename).toString();
            this.datas = JSON.parse(rawDatas);
        }
    }

} 