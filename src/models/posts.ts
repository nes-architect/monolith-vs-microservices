import fs from "fs";

interface objInterface {
    id?: string,
    createdAt?: string,
    user_id: string,
    topic_id: string,
    message: string
}

export default class Post {

    public static datas : objInterface[] = [];
    public props : objInterface;

    constructor(props : objInterface){
        this.props = props;
        if(!props.id) this.props.id = Date.now().toString();
        if(!props.createdAt) this.props.createdAt = (new Date()).toLocaleString();
    }

    public async save(){
        let index : number = Post.datas.map( u => u.id ).indexOf(this.props.id);
        if( index > -1 ){
            await Post.update(index, this.props);
            Post.write();
        }
        else{
            Post.datas.push( Object.assign( {}, this.props) );
            Post.write();
        }
    }

    public async delete(){
        let index : number = Post.datas.map( u => u.id ).indexOf(this.props.id);
        if(index === -1) return false;

        Post.datas.splice(index, 1);
        Post.write();

    }

    private static async update(indexDatas : number, datas : objInterface){
        Post.datas[indexDatas] = datas;
    }

    public static write(){
        fs.writeFileSync(`${__dirname}/../../data/${this.name}.json`, JSON.stringify(this.datas, null, "\t"));
        this.load();
    }

    public static load(){
        let filename : string = `${__dirname}/../../data/${this.name}.json`; 
        if(fs.existsSync(filename)){
            let rawDatas = fs.readFileSync(<fs.PathOrFileDescriptor>filename).toString();
            this.datas = JSON.parse(rawDatas);
        }
    }

} 