import Express from "express";
import jwt from "jsonwebtoken";

import User from "../../models/users";

// Route endpoint is stored with a Express.Router() object, so we need it to pass it to a .use() function of Express 
let router = Express.Router();

/**
 * Try to auth a user,
 *  if it works we return a token in cookie and json format
 *  Else, return an error  
 */
router.post("/", async (req : Express.Request, res : Express.Response) => {

    // Search the user match with username and password
    let items = User.datas.filter( u => (u.username === req.body.username && u.password === req.body.password) );
    if(items.length === 0) return res.status(401).send("User or password incorrect");

    // User found, sign a new token with some info from the user stored in the payload
    let user = items[0];
    let token = jwt.sign({              
        id: user.id,                    //----| This is the payload of the JWT
        username: user.username         //    |
    }, "changeme", {expiresIn: "1h"});  // Signed with the passphrase 'changeme'... with a lifetime of 1h

    // Store in cookie token
    res.cookie("token", token);
    // return the token to the client
    res.json(token);

});

/**
 * Destroy cookie with token and redirect to root url /
 */
router.get("/logout", async (req : Express.Request, res : Express.Response) => {
    res.clearCookie("token");
    res.redirect("/");
});

/**
 * Register a new user if allowed by the application (free register)
 */
router.post("/register", async (req : Express.Request, res : Express.Response) => {
    let newUser = new User(req.body);
    try{
        await newUser.save();
        res.status(201).json(newUser.props);
    }
    catch(e:any){
        res.status(500).json({message: (e||e.message)});
    }
});

// Export the router to be use with Express app
export default router;