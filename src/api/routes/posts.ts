import Express from "express";
import {isConnected} from "../../middlewares/authrequired";
import Post from "../../models/posts";

// Route endpoint is stored with a Express.Router() object, so we need it to pass it to a .use() function of Express 
let router = Express.Router();

/**
 * Return all the items of this ressource
 */
router.get("/", isConnected, async (req : Express.Request, res : Express.Response) => {
    res.json( Post.datas );
});

/**
 * Create a new item in this ressource
 */
router.post("/", isConnected, async (req : Express.Request, res : Express.Response) => {
    let item = new Post(req.body);
    item.props.user_id = res.locals.id;
    
    try{
        await item.save();
        res.status(201).json(item.props);
    }
    catch(e:any){
        res.status(500).json({message: (e||e.message)});
    }
});

/**
 * Get an item in this ressource from his ID
 */
router.get("/:ID", isConnected, async (req : Express.Request, res : Express.Response) => {
    let item = Post.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
        
    res.json( Post.datas.filter( i => i.id === req.params.ID )[0] );
});

/**
 * Update an item in this ressource from his ID
 */
router.put("/:ID", isConnected, async (req : Express.Request, res : Express.Response) => {
    let item = Post.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
    
    let itemToUpdate = new Post(req.body);
    itemToUpdate.props.id = item[0].id;
    
    await itemToUpdate.save();

    res.status(201).json( itemToUpdate.props );
});

/**
 * Delete an item in this ressource from his ID
 */
router.delete("/:ID", isConnected, async (req : Express.Request, res : Express.Response) => {
    let item = Post.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
    
    let deletedItem = new Post(item[0]);
    await deletedItem.delete();

    res.status(200).json({message: "deleted"});
});

// Export the router to be use with Express app
export default router;