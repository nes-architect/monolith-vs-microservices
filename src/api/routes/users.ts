import Express from "express";
import User from "../../models/users";
import {isConnected} from "../../middlewares/authrequired";

// Route endpoint is stored with a Express.Router() object, so we need it to pass it to a .use() function of Express 
let router = Express.Router();

/**
 * Return all the items of this ressource
 */
router.get("/", isConnected, async (req,res) => {
    res.json( User.datas );
});

/**
 * Return all info of the authenticated user, helped from the decoded token gave by idConnected function
 */
router.get("/me", isConnected, async (req,res) => {
    res.json( res.locals );
});

/**
 * Create a new item in this ressource
 */
router.post("/", isConnected, async (req,res) => {
    let item = new User(req.body);
    try{
        await item.save();
        res.status(201).json(item.props);
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }
})

/**
 * Get an item in this ressource from his ID
 */
router.get("/:ID", isConnected, async (req,res) => {
    let item = User.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
        
    res.json( User.datas.filter( i => i.id === req.params.ID )[0] );
});

/**
 * Update an item in this ressource from his ID
 */
router.put("/:ID", isConnected, async (req,res) => {
    let item = User.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
    
    let itemToUpdate = new User(req.body);
    itemToUpdate.props.id = item[0].id;
    
    try{
        await itemToUpdate.save();
        res.status(201).json( itemToUpdate.props );
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

/**
 * Delete an item in this ressource from his ID
 */
router.delete("/:ID", isConnected, async (req,res) => {
    let item = User.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
    
    let deletedItem = new User(item[0]);
    await deletedItem.delete();

    res.status(200).json({message: "deleted"});
});

export default router;