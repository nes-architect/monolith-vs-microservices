import Express from "express";
import {isConnected} from "../../middlewares/authrequired";
import Post from "../../models/posts";
import Topic from "../../models/topics";

// Route endpoint is stored with a Express.Router() object, so we need it to pass it to a .use() function of Express 
let router = Express.Router();

/**
 * Return all the items of this ressource
 */
router.get("/", isConnected, async (req,res) => {
    res.json( Topic.datas );
});

/**
 * Create a new item in this ressource
 */
router.post("/", isConnected, async (req,res) => {
    let item = new Topic(req.body);
    item.props.user_id = res.locals.id;
    
    await item.save();
    res.json(item.props);
});

/**
 * Get an item in this ressource from his ID
 */
router.get("/:ID", isConnected, async (req,res) => {
    let item = Topic.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
        
    res.json( Topic.datas.filter( i => i.id === req.params.ID )[0] );
});

/**
 * Get all posts from an item in this ressource from his item ID
 */
router.get("/:ID/posts", isConnected, async (req,res) => {
    let items = Post.datas.filter( i => i.topic_id === req.params.ID );
    res.json( items );
});

/**
 * Update an item in this ressource from his ID
 */
router.put("/:ID", isConnected, async (req,res) => {
    let item = Topic.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");
    
    let itemToUpdate = new Topic(req.body);
    itemToUpdate.props.id = item[0].id;
    
    await itemToUpdate.save();

    res.status(201).json( itemToUpdate.props );
});

/**
 * Delete an item in this ressource from his ID
 */
router.delete("/:ID", isConnected, async (req,res) => {

    let posts = Post.datas.filter( i => i.topic_id === req.params.ID);
    for(let i = 0; i < posts.length; i++){
        let p = new Post(posts[i]);
        await p.delete();
    }

    let item = Topic.datas.filter( i => i.id === req.params.ID )
    if( item.length === 0 ) return res.status(404).send("Item didn't exists.");

    let deletedItem = new Topic(item[0]);
    await deletedItem.delete();

    res.status(200).json({message: "deleted"});
});

export default router;