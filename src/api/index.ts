import Express from "express";

import auth from "./routes/auth";
import users from "./routes/users";
import topics from "./routes/topics";
import posts from "./routes/posts";

/**
 * Contains only one static function to bind all routes to the Express APP pass in parameters
 */
export default class Api {

    public static bind(app : Express.Application){
        app.use("/api/auth", auth);     // All routes for auth methods
        app.use("/api/users", users);   // All routes for users methods
        app.use("/api/topics", topics); // All routes for topics methods
        app.use("/api/posts", posts);   // All routes for posts methods
    }

}